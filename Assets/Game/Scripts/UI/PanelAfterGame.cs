﻿using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using UnityEngine.Advertisements;

public class PanelAfterGame : MonoBehaviour
{

    public Text textMoney;
    public Button shareFB;
    // Use this for initialization
    void Start()
    {
        int _currentMoney = PlayerSave.singleton.GetCurrentMoney();
        int _beforeMoney = PlayerSave.singleton.GetMoneyBeforeGame();
        int difference = _currentMoney - _beforeMoney;

        if (difference > 0)
        {
            textMoney.text = "Win money " + difference.ToString();
        }
        else
        {
            textMoney.text = "Lose money " + difference.ToString();
        }
        shareFB.onClick.AddListener(ShareFacebook);
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show("rewardedVideo", options);
    }

    private void HandleShowResult(ShowResult result)
    {       
    }

    public void ShareFacebook()
    {
        int _currentMoney = PlayerSave.singleton.GetCurrentMoney();
        int _beforeMoney = PlayerSave.singleton.GetMoneyBeforeGame();
        int difference = _currentMoney - _beforeMoney;
        FB.ShareLink(new System.Uri(Url.facebook), "Teen Patti War", "I played in Teen Patti War and won "+ difference.ToString());
    }


}
