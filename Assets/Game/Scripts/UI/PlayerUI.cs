﻿using System.Collections;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using Facebook.Unity;

public class PlayerUI : MonoBehaviour
{

    public  int IdOrder;   
    public bool IsFull;
    public Sprite  defaultAvatar;

    private Image avatarPlayer;
    private Image avatarCircul;
    private Button avatarButton;
    private Image avatarFilled;
    private Image[] cards;
    private Text money;
    private Text infoText, CardCombinationText, nameText;
    private SpriteAtlas cardAtlas;

    private Text textMovingMoney;
    private RectTransform movingMoney;
    private RectTransform closeMovingCard;
    private RectTransform startCardPositionMove;
    private RectTransform finishMoneyPosition;
    private CanvasGroup myCanvasGroup;
    private PlayerManagerPun currentPlayer;
    private Color colorPaked = new Color(.2f, .2f, .2f);
    private GameObject panelPlayerInfo;
    public void Awake()
    {
        myCanvasGroup = GetComponent<CanvasGroup>();
        startCardPositionMove = transform.parent.Find("PositionStartCard").GetComponent<RectTransform>();
        finishMoneyPosition = transform.parent.Find("PositionFinishMoney").GetComponent<RectTransform>();
        cardAtlas = Resources.Load<SpriteAtlas>("Cards");
        avatarPlayer = transform.Find("Avatar/AvatarFacebook").GetComponent<Image>();
        avatarCircul = transform.Find("AvatarCircul").GetComponent<Image>();
        avatarCircul.enabled = false;
        money = transform.Find("ImageMoney").Find("TextMoney").GetComponent<Text>();       
        infoText = transform.Find("InfoText").GetComponent<Text>();
        CardCombinationText = transform.Find("InfoCardText").GetComponent<Text>();
        nameText = transform.Find("InfoNamePlayer").GetComponent<Text>();
        cards = transform.Find("Cards").GetComponentsInChildren<Image>();      
        avatarFilled = Instantiate(avatarPlayer, transform);
        avatarFilled.transform.localPosition = avatarPlayer.transform.localPosition;
        avatarFilled.transform.localScale = Vector3.one;
        avatarFilled.type = Image.Type.Filled;       
        GameObject clone = Instantiate(Resources.Load<GameObject>("CardClose"), transform);     
        closeMovingCard = clone.GetComponent<RectTransform>();
        closeMovingCard.sizeDelta = cards[0].rectTransform.sizeDelta;    
        movingMoney = Instantiate(Resources.Load<GameObject>("GiveMoney"), transform).GetComponent<RectTransform>();
        textMovingMoney = movingMoney.GetComponentInChildren<Text>();
        panelPlayerInfo = transform.Find("PanelPlayerInfo").gameObject;
        avatarPlayer.raycastTarget = true;
        avatarButton = avatarPlayer.gameObject.AddComponent<Button>();
        avatarButton.onClick.AddListener(OnAvatarButton);
        panelPlayerInfo.SetActive(false);
        myCanvasGroup.alpha = 1;
        ClearUI();
    }   

    public void SetIsFull(bool _value)
    {
         IsFull = _value;       
    }

    public bool GetIsFull()
    {
        return IsFull;
    }
    public void OnAvatarButton()
    {
        panelPlayerInfo.SetActive(true);
        if (!currentPlayer.photonView.isMine)
        {
                        
        }      
    }

    public void Init(PlayerManagerPun _currentPlayer)
    {
        currentPlayer = _currentPlayer;
        avatarPlayer.enabled = true;
        avatarCircul.enabled = true;
        if (!string.IsNullOrEmpty(currentPlayer.playerData.FacebookId))
        {
            FB.API(currentPlayer.playerData.FacebookId + "/picture?width=50&height=50", HttpMethod.GET, AvatarCallBack);
        }
        else
        {
            avatarPlayer.sprite = defaultAvatar;
            avatarFilled.sprite = defaultAvatar;
        }
        int level = (int)currentPlayer.playerData.experience / 100;
        panelPlayerInfo.transform.Find("TextLevel").GetComponent<Text>().text = level.ToString();
        panelPlayerInfo.transform.Find("TextName").GetComponent<Text>().text = currentPlayer.playerData.NamePlayer;
        panelPlayerInfo.transform.Find("TextEarnings").GetComponent<Text>().text = currentPlayer.playerData.Money.ToString();
      //  panelPlayerInfo.transform.Find("TextLocation").GetComponent<Text>().text = currentPlayer.playerData.Location;
      //  panelPlayerInfo.transform.Find("TextRank").GetComponent<Text>().text = currentPlayer.playerData.experience.ToString();
        money.transform.parent.gameObject.SetActive(true);
        nameText.text = _currentPlayer.playerData.NamePlayer;
        GiveTextMoney(_currentPlayer.playerData.Money, 0);
    }

    private void AvatarCallBack(IGraphResult result)
    {
        avatarPlayer.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 50, 50), new Vector2(0.5f, 0.5f));
        avatarFilled.sprite = avatarPlayer.sprite;
    }

    public void ClearUI()
    {        
        StopAllCoroutines();
        foreach (Image item in cards)
        {
            item.enabled = false;
        }
        movingMoney.gameObject.SetActive(false);
        closeMovingCard.gameObject.SetActive(false);
        avatarFilled.enabled = false;
        money.transform.parent.gameObject.SetActive(false);
        avatarPlayer.enabled = false;
        avatarCircul.enabled = false;
        nameText.text = "";
        money.text = "";
        infoText.text = "";
        currentPlayer = null;
    }

    public void PlayerEmpty()
    {
        myCanvasGroup.alpha = 0.4f;
        money.text = "";
        infoText.text = "";
    }

    public void ReloadUI()
    {
        infoText.text = "";
        SetCardCombinationText(eCombination.Empty);            
        avatarPlayer.color =  Color.white;
        for (int i=0;i< cards.Length;i ++)
        {
            cards[i].color = Color.white;
            cards[i].enabled = false;
        }
    }

    public void SetPacked()
    {
        infoText.text = "Packed";
        avatarPlayer.color = colorPaked;
        foreach (var item in cards)
        {
            item.color = colorPaked;
        }
       // StartCoroutine("PackedCards");      
        SetCardCombinationText(eCombination.Empty);
        StopStep();
    }

    public void SetWinText()
    {
        infoText.text = "win";
    }

    public void SetInfoBlind(bool isSeen)
    {
        if (isSeen)
            infoText.text = "Seen";
        else
            infoText.text = "Blind";
    }

    public void SetInfoEmpty()
    {    
            infoText.text = "";       
    }

    public void SetCardCombinationText(eCombination _combination)
    {
        if (_combination != eCombination.Empty)
            CardCombinationText.text = _combination.ToString().ToUpper();
        else
            CardCombinationText.text = "";
    }

    public void GiveTextMoney(int currentMoney,int giveMovey)
    {
        money.text = currentMoney.ToString();
        if(giveMovey != 0)
        {
            textMovingMoney.text = giveMovey.ToString();
            StartCoroutine("MovingGiveMoney");
        }       
    }

    public void WinTextMoney(int currentMoney, int giveMovey)
    {
        textMovingMoney.text = giveMovey.ToString();
        StartCoroutine("MovingWinMoney", currentMoney);
    }

    public void SetCard(CardData card, int numCard, bool staticCard)
    {
        if (card.isClose)
        {
            cards[numCard].sprite = cardAtlas.GetSprite("Back");
            if (!staticCard)
                StartCoroutine("MovingCloseCardToPlayer", numCard);
        }
        else
        {
            if (card.rankCard == 0)
                cards[numCard].sprite = cardAtlas.GetSprite("Back");
            else
                cards[numCard].sprite = cardAtlas.GetSprite(card.suitCard.ToString() + card.rankCard.ToString());
        }
    }

    public void SetCurrentStep()
    {
        StartCoroutine("CurrentStep");
    }    

    public void StopStep()
    {
        StopCoroutine("CurrentStep");
        avatarFilled.enabled = false;
    }

    private void AutoStopStep()
    {
        currentPlayer.photonView.RPC("PackPlayer", PhotonTargets.All);
    }

    private IEnumerator PackedCards()
    {
        RectTransform parentCards = cards[0].transform.parent.GetComponent<RectTransform>();
        Vector3 startPos = parentCards.localPosition;
        for (float distance = Vector3.Distance(parentCards.position, finishMoneyPosition.position); distance >= 2;)
        {
            parentCards.position = Vector3.MoveTowards(parentCards.position, finishMoneyPosition.position, 12);
            parentCards.Rotate(0,0,-8);
            distance = Vector3.Distance(parentCards.position, finishMoneyPosition.position);
            yield return new WaitForFixedUpdate();
        }
        parentCards.localPosition = startPos;
        parentCards.localEulerAngles = Vector3.zero;
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].enabled = false;
        }
    }

    private IEnumerator MovingWinMoney(int currentMoney)
    {
        movingMoney.gameObject.SetActive(true);
        movingMoney.position = finishMoneyPosition.position; 
        for (float distance = Vector3.Distance(movingMoney.position, transform.position); distance >= 2;)
        {
            movingMoney.position = Vector3.MoveTowards(movingMoney.position, transform.position, 15);
            distance = Vector3.Distance(movingMoney.position, transform.position);
            yield return new WaitForFixedUpdate();
        }
        money.text = currentMoney.ToString();
        movingMoney.gameObject.SetActive(false);
    }

    private IEnumerator MovingGiveMoney()
    {
        movingMoney.gameObject.SetActive(true);
        movingMoney.position = money.rectTransform.position;
        for (float distance = Vector3.Distance(movingMoney.position, finishMoneyPosition.position); distance >= 2;)
        {
            movingMoney.position = Vector3.MoveTowards(movingMoney.position, finishMoneyPosition.position, 15);
            distance = Vector3.Distance(movingMoney.position, finishMoneyPosition.position);
            yield return new WaitForFixedUpdate();
        }
        movingMoney.gameObject.SetActive(false);
    }

    private IEnumerator CurrentStep()
    {
        avatarFilled.color = Color.green;
        avatarFilled.enabled = true;
        float step = (float)60 / 100;       

        for (float i=0; i <=1; i += 0.012f)
        {
           if(i > 0.46f && i< 0.51f)
                avatarFilled.color = Color.yellow;
            if (i > 0.78f && i < 0.82f)
                avatarFilled.color = Color.red;
            avatarFilled.fillAmount = i;
            yield return new WaitForSeconds(step);
        }
        avatarFilled.enabled = false;
        AutoStopStep();
    }

    private IEnumerator MovingCloseCardToPlayer(int numCard)
    {
        closeMovingCard.gameObject.SetActive(true);
        closeMovingCard.position = startCardPositionMove.position;
        closeMovingCard.eulerAngles = cards[numCard].transform.eulerAngles;
        for (float distance = Vector3.Distance(closeMovingCard.position,transform.position); distance>=2;)
        {
            closeMovingCard.position = Vector3.MoveTowards(closeMovingCard.position, cards[numCard].transform.position,50);
            distance = Vector3.Distance(closeMovingCard.position, cards[numCard].transform.position);
            yield return new WaitForFixedUpdate();
        }
        closeMovingCard.gameObject.SetActive(false);
        cards[numCard].enabled = true;
    }
}
