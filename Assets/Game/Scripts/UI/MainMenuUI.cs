﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    public Text monetPlayer;
    public Text expPlayerText;
    public Slider explayerSlider;
    public Image avatarImage;
    public Sprite avatarDefault;
    // Use this for initialization
    void Start()
    {       
        if (!string.IsNullOrEmpty(PlayerSave.singleton.GetFacebookId()))
        {
            avatarImage.sprite = Sprite.Create(PlayerSave.singleton.Avatar, new Rect(0, 0, 50, 50), new Vector2(0.5f, 0.5f));
        }
        else
        {          

        }
        monetPlayer.text = PlayerSave.singleton.GetCurrentMoney().ToString();
        int _exp = PlayerSave.singleton.GetExp();
        int level = (int)_exp / 100;
        explayerSlider.value = _exp - (level * 100);
        expPlayerText.text = level.ToString();
    }

	public void RefreshMoneyUI()
	{
		monetPlayer.text = PlayerSave.singleton.GetCurrentMoney().ToString();
	}


}
