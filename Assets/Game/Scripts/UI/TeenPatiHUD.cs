﻿using UnityEngine;
using UnityEngine.UI;

public class TeenPatiHUD : MonoBehaviour
{
    public Slider playerExp;
   
    public Text playerLevel; 
    public Text totalBet;
    public Text bootAmount;
    public Text blindLimit;
    public Text chalLimit;
    public Text potLimit;
    [SerializeField]
    private Text textGlobalInfo;  

    public void SetTextGlobalInfo(string _text)
    {
        textGlobalInfo.enabled = true;
        textGlobalInfo.text = _text;
        Invoke("DeactivateTextInfo",3);
    }
   
    public void TotalBoot(string _text)
    {
        totalBet.text = _text;
    }  

    private void DeactivateTextInfo()
    {
        textGlobalInfo.enabled = false;
    }
}
