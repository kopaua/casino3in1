﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class ScorePanelUI : MonoBehaviour
{
    private Transform[] content = new Transform[9];
    private bool isTup;
    public GameObject panelLoad;
#if UNITY_IOS
     private string gameId = "com.GameTitans.TeenPatiLeaderboard";
#elif UNITY_ANDROID
    private string gameId = "CgkIlbnVm-MNEAIQAA";
#endif
    // Use this for initialization

    void Start()
    {
        if (PlayerSave.singleton.GetExp() == 0)
            return;

        if (Social.localUser.authenticated)
        {
            Social.ReportScore(PlayerSave.singleton.GetExp(), gameId, (bool success) =>{});
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)                
                    Social.ReportScore(PlayerSave.singleton.GetExp(), gameId, (bool successReport) => {});                
            });
        }
    }

    public void ShowIntegrate()
    {
        if (isTup)
            return;

       // Invoke("CanTup",12);
        panelLoad.SetActive(true);       
        isTup = true;
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
            CanTup();
        }          
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Social.ShowLeaderboardUI();
                    Invoke("CanTup", 4);
                }
                else
                    CanTup();
            });
        }
    }

    private void CanTup()
    {
        panelLoad.SetActive(false);
        isTup = false;
    }

    private void CreteBoard()
    {
        GameObject cloneInfo = transform.Find("BgWorld/Content/PlayerInfo").gameObject;
        for (int i = 0; i < 9; i++)
        {
            GameObject _clone = Instantiate(cloneInfo, cloneInfo.transform.parent);
            content[i] = _clone.transform;
            _clone.transform.Find("TextNum").GetComponent<Text>().text = (i + 1).ToString();
            _clone.transform.Find("TextName").GetComponent<Text>().text = "";
            _clone.transform.Find("TextScore").GetComponent<Text>().text = "";          
        }
        cloneInfo.SetActive(false);      
    }

}
