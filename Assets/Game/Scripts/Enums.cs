﻿public enum eCardSuit
{
    Clubs = 1,
    Diamonds = 2,
    Hearts = 3,
    Spade = 4,
    Joker = 5
}

public enum ePlayerType
{
    Empty =0,
    Bot = 1,
    PlayerStartGame = 2,
    PlayerInRoom = 3
}

public enum eCombination
{
    Empty = 0,
    HighCard = 1,
    Pair = 2,
    Flush = 3,
    Sequence = 4,
    StraightFlush = 5,
    Trail =6
}


public enum eTable
{
    None,
    Standart,
    NoLimit,
    Joker
}

