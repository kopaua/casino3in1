﻿using UnityEngine;
using System;

[Serializable]
public struct PlayerSaveData
{
    public string namePlayer;
    public string idFacebook;
    public string location;
    public int moneyPlayer;
    public int moneyPlayerBeforeGame;
    public int score;
    public int experience;
}

public class PlayerSave : MonoBehaviour
{
    public static PlayerSave singleton;
    
    private PlayerSaveData playerSaveData;
    private const int startMoney = 100000;
    public Texture2D Avatar;
    public eTable currentTable;

    // Use this for initialization
    void Awake()
    {            
        if (singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
       //  PlayerPrefs.DeleteAll();    
        string save = PlayerPrefs.GetString("SavePlayer", "");      
        if (string.IsNullOrEmpty(save))
        {
            playerSaveData = new PlayerSaveData();
            playerSaveData.moneyPlayer = startMoney;
            playerSaveData.namePlayer = "new player";
        }
        else
        {
            playerSaveData = JsonUtility.FromJson<PlayerSaveData>(save);
        }
    }

    public int GetCurrentMoney()
    {
        return playerSaveData.moneyPlayer;
    }

    public int GetMoneyBeforeGame()
    {
        return playerSaveData.moneyPlayerBeforeGame;
    }

    public string GetCurrentNamey()
    {
        return playerSaveData.namePlayer;
    }

    public string GetFacebookId()
    {
        return playerSaveData.idFacebook;
    }


    public string GetLocation()
    {
        return playerSaveData.location;
    }

    public void SaveNewName(string newName)
    {       
        playerSaveData.namePlayer = newName;
        PlayerPrefs.SetString("SavePlayer", JsonUtility.ToJson(playerSaveData));
    }

    public void SaveNewMoney(int newCountMoney)
    {
        playerSaveData.moneyPlayer = newCountMoney;
        PlayerPrefs.SetString("SavePlayer", JsonUtility.ToJson(playerSaveData));
    }

    public void SaveFacebookId(string fcID)
    {
        playerSaveData.idFacebook = fcID;
    }

    public void Savelocation(string _location)
    {
        playerSaveData.location = _location;
    }

    public void SaveMoneyBeforeGame()
    {
        playerSaveData.moneyPlayerBeforeGame = playerSaveData.moneyPlayer;

    }

    public void ClearMoneyBeforeGame()
    {
        playerSaveData.moneyPlayerBeforeGame = 0;

    }

    public int GetExp()
    {
        return playerSaveData.experience;
    }

    public void AddExp(int totalBet)
    {
        int AddEx = 0;
        if (playerSaveData.experience < 1000)
            AddEx = totalBet / 10;
        else if (playerSaveData.experience < 20000 && playerSaveData.experience > 1000)
            AddEx = totalBet / 100;
        else if (playerSaveData.experience > 20000)
            AddEx = totalBet / 500;

        if (AddEx > 0)
            playerSaveData.experience += AddEx;
    }

}
