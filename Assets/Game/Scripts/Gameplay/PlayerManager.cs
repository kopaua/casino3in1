﻿using UnityEngine;
using System;
using System.Linq;
using UnityEngine.Networking;



public class PlayerManager : NetworkBehaviour
{
    [SyncVar]
    public PlayerData playerData;

    public int blindLimit;
    public int startBoot;
    public int chalLimit;
    public int potLimit;

    private PlayerUI myUI;
    private LocalPlayer localPlayer;
    private TeenPattiManager managerMain;
    private TeenPatiHUD hud;
    private PlayerSave _playerSave;

    public void Start()
    {
        _playerSave = FindObjectOfType<PlayerSave>();
        playerData.currentCards = new CardData[3];
        managerMain = FindObjectOfType<TeenPattiManager>();
        if (isLocalPlayer)
        {
            playerData.Money = _playerSave.GetCurrentMoney();
            playerData.NamePlayer = _playerSave.GetCurrentNamey();
            localPlayer = gameObject.AddComponent<LocalPlayer>();          
            CmdInitPLayerInManager(playerData.Money, playerData.NamePlayer);           
        }
        else
        {
            if (!hasAuthority)
                CmdISyncGameInManager();
        }
    }   

    private void InitHud()
    {
        hud = FindObjectOfType<TeenPatiHUD>();
        hud.blindLimit.text = blindLimit.ToString();
        hud.chalLimit.text = chalLimit.ToString();
        hud.potLimit.text = potLimit.ToString();
    }
    [Command]
    public void CmdClearPlayerFromGame()
    {
        playerData.playerType = ePlayerType.Empty;
        myUI.ClearUI();
        RpcClear();
    }

    [Command]
    public void CmdEndTypeChat(string _text)
    {
        managerMain.AddChat(_text);
    }

    [Command]
    public void CmdDisconnect()
    {
        managerMain.PlayerDisconnected(this);
    }

   [Command]
    public void CmdInitPLayerInManager(int _money,string _name)
    {
        playerData.Money = _money;
        playerData.NamePlayer = _name;
        RpcNameAndMoney(playerData.Money, playerData.NamePlayer);
        FindObjectOfType<TeenPattiManager>().InitPlayer(this);
    }
    [ClientRpc]
    public void RpcNameAndMoney(int _money, string _name)
    {
        playerData.Money = _money;
        playerData.NamePlayer = _name;
    }

    [Command]
    public void CmdISyncGameInManager()
    {
        FindObjectOfType<TeenPattiManager>().SyncGame(this);
    }

    [Command]
    public void CmdDecreaseBet()
    {
        playerData.IsDoubleBoot = false; 
    }

    [Command]
    public void CmdIncreaseBet()
    { 
       if(CanIncreaseBoot())
        {
            playerData.IsDoubleBoot = true;
        }
    }

    [Command]
    public void CmdPack()
    {
        RpcSetPacked();
        RpcStopStep();
        managerMain.PackPlayer();
    }

    [Command]
    public void CmdChaal()
    {
        RpcStopStep();
        managerMain.Chaal(this);
    }  

    [Command]
    public void CmdSideShow()
    {
        RpcStopStep();
        managerMain.StartSideShow(this);
    }

    [Command]
    public void CmdSeenCard()
    {
        RpcSetSeenCardTrue();
    }

    [Command]
    public void CmdAcceptSideShow()
    {
        managerMain.AcceptSideShow(this);
    }

    [Command]
    public void CmdDeclineSideShow()
    {
       managerMain.DeclineSideShow(this);
    }

    [Command]
    public void CmdInitPlayerData(string freeUI, int boot, int blint, int chal, int pot)
    {
        startBoot = boot;
        blindLimit = blint;
        chalLimit = chal;
        potLimit = pot;
        InitHud();
        InitUI(freeUI);
        playerData.playerType = ePlayerType.PlayerInRoom;
        RpcInitPlayerData(freeUI,  boot,  blint,  chal,  pot);
    }

    [ClientRpc]
    public void RpcNewChat(string textChat)
    {
        if (localPlayer)
            localPlayer.NewChatText(textChat);
    }  

    [ClientRpc]
    public void RpcClear()
    {
        ClearFromGame();
    }

    public void ClearFromGame()
    {
        playerData.playerType = ePlayerType.Empty;
        myUI.ClearUI();
        if (localPlayer)
        {
            if (isServer && hasAuthority)
            {
                NetworkManager.singleton.StopHost();
            }
            else
            {
                NetworkManager.singleton.StopClient();
            }
        }
        //UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        Destroy(gameObject);
    }

    [ClientRpc]
    public void RpcShowCardForAll()
    {
        playerData.IsSeenCard = true;
        myUI.SetInfoBlind(playerData.IsSeenCard);
        StopStep();
        ShowCard();
    }

   [ClientRpc]
    public void RpcInitPlayerData(string freeUI,int boot, int blint, int chal,int pot)
    {
        startBoot = boot;
        blindLimit = blint;
        chalLimit = chal;
        potLimit = pot;
        InitHud();
        InitUI(freeUI);      
        playerData.playerType = ePlayerType.PlayerInRoom;     
    } 

   [ClientRpc]
    public void RpcInitUI(string currentUI)
    {
        InitUI(currentUI);          
    } 

    [ClientRpc]
    public void RpcSetNewBoot(int boot)
    {
        playerData.currentBootPlayer = boot;
        if (localPlayer)
            localPlayer.TextCurrebyBoot(playerData.currentBootPlayer);
    }

    [ClientRpc]
    public void RpcSetTotal(int total)
    {
        if (isLocalPlayer)
            hud.TotalBoot(total.ToString());
    }

    [ClientRpc]
    public void RpcGlobalInfo(string  _globalInfo)
    {
        if (isLocalPlayer)
            hud.SetTextGlobalInfo(_globalInfo);
    }

    [ClientRpc]
    public void RpcSetInGame()
    {       
        playerData.playerType = ePlayerType.PlayerStartGame;
          if(localPlayer)
            localPlayer.TextShowButtonToSeen();

        playerData.IsPacked = false;
        playerData.IsSeenCard = false;
        playerData.currentCombination = eCombination.Empty;
        myUI.ReloadUI();
    }

    [ClientRpc]
    public  void RpcReloadPlayerToNewGame()
    {
        if (localPlayer)
            localPlayer.TextShowButtonToSeen();

        playerData.IsPacked = false;
        playerData.IsSeenCard = false;
        playerData.currentCombination = eCombination.Empty;
        myUI.ReloadUI();
    }

    [ClientRpc]
    public void RpcGiveMoney(int bootAmount)
    {
        playerData.Money -= bootAmount;
        myUI.GiveTextMoney(playerData.Money, bootAmount);
        if (localPlayer)
        {
            _playerSave.SaveNewMoney(playerData.Money);
        }
    }

    [ClientRpc]
    public void RpcStartStep()
    {
        if (isLocalPlayer)
            localPlayer.ActivateAllButtons();

        playerData.IsDoubleBoot = false;
        playerData.step++;
        if (blindLimit != 0 && playerData.step >= blindLimit && !playerData.IsSeenCard)
        {          
            if (isLocalPlayer)
            {
                CmdSeenCard();
                localPlayer.SeenCardText();
                localPlayer.SetLocalInfoText("blind Limit  4 turns");               
            }
        }
        myUI.SetCurrentStep();

        if (playerData.playerType == ePlayerType.Bot)
        {
          //int randomTime = UnityEngine.Random.Range(1, timeStep / 2);
         // Invoke("BotLogicStep", randomTime);
        }
    }

   [ClientRpc]
    private  void RpcStopStep()
    {
        myUI.StopStep();
        if (isLocalPlayer)
            localPlayer.DeactivateAllButtons();
    }  

    [ClientRpc]
    public void RpcSetNewCards(string card,int numCard)
    {
        CardData newCard = JsonUtility.FromJson<CardData>(card);
        playerData.currentCards[numCard] = newCard;
        myUI.SetCard(newCard, numCard,false);
        if (numCard == 2)
        {
            LastCardInHand();
        }
    }

    [ClientRpc]
    public void RpcSetSeenCardTrue()
    {
        SetSeenCardTrue();
    }

    [ClientRpc]
    public void RpcSetPacked()
    {  
        StopStep();
        playerData.IsPacked = true;
        myUI.SetPacked();
    }

    [ClientRpc]
    public void RpcWinHand(int totalBet)
    {
        myUI.SetWinText();
        playerData.Money += totalBet;
        myUI.WinTextMoney(playerData.Money, totalBet);
        StopStep();
        if (localPlayer)
        {
            _playerSave.SaveNewMoney(playerData.Money);
        }
    }

    [ClientRpc]
    public void RpcStartSideShow(string nameCalledPlayer)
    {
        if (isLocalPlayer)
        {
            localPlayer.StarSideShow();
        }        
    }
    [ClientRpc]
    public void RpcShowCardsOpponet(string netId)
    {
        if (isLocalPlayer)
        {
            FindObjectsOfType<PlayerManager>().First(x => x.playerData.NamePlayer == netId).ShowCard();
        }
    }        

    private void InitUI(string currentUI)
    {
        if (myUI == null)
            myUI = GameObject.Find(currentUI).GetComponent<PlayerUI>();
        //myUI.Init(playerData.NamePlayer, playerData.Money, "");
        for (int i = 0; i < playerData.currentCards.Length; i++)
        {
            myUI.SetCard(playerData.currentCards[i], i, true);
        }
        if (playerData.playerType == ePlayerType.PlayerStartGame)
            myUI.SetInfoBlind(playerData.IsSeenCard);
        else
            myUI.SetInfoEmpty();
    }   

    public int GetIdOrderUI()
    {
        return myUI.IdOrder;

    }

    public void SeenCard()
    {
        if (!GetIsSeenCard())
        {
            CmdSeenCard();
        }
        else
        {
            CmdSideShow();
        }       
    }

    public CardData[] GetCurrentCards()
    {
        return playerData.currentCards;
    }

    public string GetNameUI()
    {
        return myUI.name;
    }

    public int GetMoney()
    {
        return playerData.Money;
    }

    public bool GetIsDouble()
    {
        return playerData.IsDoubleBoot;
    }

    public bool GetIsPacked()
    {
        return playerData.IsPacked;
    }

    public bool GetIsSeenCard()
    {
        return playerData.IsSeenCard;
    }

    public ePlayerType GetPlayerType()
    {
        return playerData.playerType;
    }

    public int GetCurrentBoot()
    {
        return playerData.currentBootPlayer;
    }

    public bool CanIncreaseBoot()
    {
        int newBoot = playerData.currentBootPlayer * 2;
        if (newBoot <= chalLimit && playerData.Money >= newBoot)
            return true ;
        else
            return false;
    }

    private void ShowCard()
    {
        for (int i = 0; i < playerData.currentCards.Length; i++)
        {
            playerData.currentCards[i].isClose = false;
            myUI.SetCard(playerData.currentCards[i], i,false);
        }
        myUI.SetCardCombinationText(playerData.currentCombination);
    }

    private void SetSeenCardTrue()
    {
        playerData.IsSeenCard = true;       
        myUI.SetInfoBlind(playerData.IsSeenCard);
        if (isLocalPlayer)
        {
            localPlayer.SeenCardText();
            ShowCard();
        }          
    }

    private void StopStep()
    {
        myUI.StopStep();
        if (isLocalPlayer)
            localPlayer.DeactivateAllButtons();
    }  
   
    private void LastCardInHand()
    {
        myUI.SetInfoBlind(playerData.IsSeenCard);
        playerData.currentCards = playerData.currentCards.OrderBy(x => x.rankCard).ToArray();
        playerData.IsDoubleBoot = false;
        playerData.step = 0;
        playerData.currentCombination = CardCombination.GetCombinationFromCard(playerData.currentCards);
    }  

   

    /*
    private void BotLogicStep()
    {
        TeenPattiManager manager = FindObjectOfType<TeenPattiManager>();
        int random = UnityEngine.Random.Range(0, 100);
        if (random > 75)
        {    
            manager.PackPlayer(this);
        }
        else
        {
            random = UnityEngine.Random.Range(0, 100);
            if (!playerData.IsSeenCard && random > 50)
            {
                SetSeenCardTrue();
            }
            random = UnityEngine.Random.Range(0, 100);
            if (random > 50)
            {
                IncreaseBoot();
            }             
            manager.Chaal(this);
        }
        StopStep();
    }
    */


}
