﻿using UnityEngine;
using System;
using System.Linq;

[Serializable]
public struct PlayerData
{
    public ePlayerType playerType;
    public eCombination currentCombination;
    public int Money;
    public int step;
    public int currentBootPlayer;
    public int experience;
    public bool IsDoubleBoot;
    public bool IsSeenCard;
    public bool IsLocalPlayer;
    public bool IsPacked;
    public string NamePlayer;
    public string FacebookId;
    public string Location;
    public CardData[] currentCards;
}

public class PlayerManagerPun : Photon.PunBehaviour
{
    public PlayerData playerData;
    public bool isDisconnect;
    private PlayerUI myUI;
    private LocalPlayerPun localPlayer;
    private TeenPattiPhoton managerMain;
    private TeenPatiHUD hud;
    private PlayerSave _playerSave;
    private TableInfo tableInfo;

    public override void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.sender.TagObject = gameObject;
        base.OnPhotonInstantiate(info);
        _playerSave = FindObjectOfType<PlayerSave>();
        playerData.currentCards = new CardData[3];
        managerMain = FindObjectOfType<TeenPattiPhoton>();
        if (photonView.isMine)
        {
            GameObject.Find("Table").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>(PlayerSave.singleton.currentTable.ToString());
            playerData.FacebookId = PlayerSave.singleton.GetFacebookId();
            playerData.Money = _playerSave.GetCurrentMoney();        
            playerData.NamePlayer = _playerSave.GetCurrentNamey();
            playerData.experience = _playerSave.GetExp();
            playerData.Location = _playerSave.GetLocation();
            localPlayer = gameObject.AddComponent<LocalPlayerPun>();
            localPlayer.SetTextMoneyTopBar(playerData.Money.ToString());
            photonView.RPC("InitPLayerInManager", PhotonTargets.MasterClient, JsonUtility.ToJson(playerData));
        }
    }

    public override void OnLeftRoom()
    {
        playerData.playerType = ePlayerType.Empty;
        myUI.ClearUI();
        base.OnLeftRoom();
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public override void OnDisconnectedFromPhoton()
    {
        playerData.playerType = ePlayerType.Empty;
        myUI.ClearUI();
        base.OnDisconnectedFromPhoton();        
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);     
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
         base.OnMasterClientSwitched(newMasterClient);
    }    

    [PunRPC]
    public void InitPLayerInManager( string playerSync)
    {
        playerData = JsonUtility.FromJson<PlayerData>(playerSync);
    
        if (PhotonNetwork.isMasterClient)
            FindObjectOfType<TeenPattiPhoton>().InitPlayer(this);
    }

    [PunRPC]
    public void ChaalPlayer()
    {
        StopStep();
        if (PhotonNetwork.isMasterClient)
        {
            managerMain.Chaal(this);
        }
    }

    [PunRPC]
    public void PackPlayer()
    {
        StopStep();
        playerData.IsPacked = true;
        myUI.SetPacked();
        if (PhotonNetwork.isMasterClient)
        {
            managerMain.PackPlayerFromClient();
        }
    }

    [PunRPC]
    public void PackPlayerByServer()
    {
        StopStep();
        playerData.IsPacked = true;
        myUI.SetPacked();
    }  

    [PunRPC]
    public void EndTypeChat(string _text)
    {
        managerMain.AddChat(_text,myUI.IdOrder);
    }

    [PunRPC]
    public void Disconnect()
    {
        isDisconnect = true;
        if (PhotonNetwork.isMasterClient)
            managerMain.PlayerDisconnected(this);
    }  

    [PunRPC]
    public void DecreaseBet()
    {
        playerData.IsDoubleBoot = false;
    }
    [PunRPC]
    public void IncreaseBet()
    {
        if (CanIncreaseBoot())
        {
            playerData.IsDoubleBoot = true;
        }
    }

    [PunRPC]
    public void CallSideShow()
    {
        StopStep();
        if (PhotonNetwork.isMasterClient)
        {
            managerMain.StartSideShow(this);
        }
    }

    [PunRPC]
    public void AcceptSideShow()
    {
        managerMain.AcceptSideShow(this);
    }

    [PunRPC]
    public void DeclineSideShow()
    {
        managerMain.DeclineSideShow(this);
    }  

    [PunRPC]
    public void NewChat(string _textAdd, int uiOrder)
    {
        if (photonView.isMine)
            localPlayer.NewChatText(_textAdd, uiOrder);
    }
    [PunRPC]
    public void ClearFromGame()
    {
        playerData.playerType = ePlayerType.Empty;
        myUI.ClearUI();     
        if (photonView.isMine)
        {
            PhotonNetwork.LeaveRoom();
          //  PhotonNetwork.Disconnect();          
        }
    }

    [PunRPC]
    public void ShowCardForAll()
    {
        playerData.IsSeenCard = true;
        myUI.SetInfoBlind(playerData.IsSeenCard);
        StopStep();
        ShowCard();
    }

    [PunRPC]
    public void InitTableData(string _tableInfo, int total)
    {
        TableInfo _tableInfoSync = JsonUtility.FromJson<TableInfo>(_tableInfo);
        tableInfo = _tableInfoSync;
        //  tableInfo.startBoot = _tableInfoSync.startBoot;
        // tableInfo.blindLimit = _tableInfoSync.blindLimit;
        //  tableInfo.chalLimit = _tableInfoSync.chalLimit;
        //  tableInfo.potLimit = _tableInfoSync.potLimit;
        hud = FindObjectOfType<TeenPatiHUD>();
        if (tableInfo.blindLimit >0)
        {
            hud.blindLimit.text = tableInfo.blindLimit.ToString();
            hud.chalLimit.text = tableInfo.chalLimit.ToString();
            hud.potLimit.text = tableInfo.potLimit.ToString();
        }
        else
        {
            hud.blindLimit.text = "no limit".ToUpper();
            hud.chalLimit.text = "no limit".ToUpper();
            hud.potLimit.text = "no limit".ToUpper();
        }
       
        playerData.playerType = ePlayerType.PlayerInRoom;
        hud.TotalBoot(total.ToString());
    }   

    [PunRPC]
    public void InitUI(string currentUI,string playerSync)
    {              
        playerData = JsonUtility.FromJson<PlayerData>(playerSync);
        if (myUI == null)
            myUI = GameObject.Find(currentUI).GetComponent<PlayerUI>();
     
        myUI.Init(this);
        for (int i = 0; i < playerData.currentCards.Length; i++)
        {
            myUI.SetCard(playerData.currentCards[i], i, playerData.currentCards[i].isClose);
        }
        if (playerData.playerType == ePlayerType.PlayerStartGame)
            myUI.SetInfoBlind(playerData.IsSeenCard);
        else
            myUI.SetInfoEmpty();
    }

    [PunRPC]
    public void SetTotal(int total)
    {
        if (photonView.isMine)
            hud.TotalBoot(total.ToString());
    }

    [PunRPC]
    public void GlobalInfo(string _globalInfo)
    {
        if (photonView.isMine)
            hud.SetTextGlobalInfo(_globalInfo);
    }

    [PunRPC]
    public void SetInGame()
    {       
        if (photonView.isMine)
            localPlayer.TextShowButtonToSeen();

        playerData.playerType = ePlayerType.PlayerStartGame;
        playerData.IsPacked = false;
        playerData.IsSeenCard = false;
        playerData.currentCombination = eCombination.Empty;
        myUI.ReloadUI();
    }

    [PunRPC]
    public void GiveMoney(int bootAmount)
    {
        playerData.Money -= bootAmount;
        myUI.GiveTextMoney(playerData.Money, bootAmount);
        if (photonView.isMine)
        {
            localPlayer.SetTextMoneyTopBar(playerData.Money.ToString());
            _playerSave.SaveNewMoney(playerData.Money);
        }
    }

    [PunRPC]
    public void StartStep(int newBoot)
    {
        playerData.currentBootPlayer = newBoot;
        playerData.IsDoubleBoot = false;
        playerData.step++;
        if (photonView.isMine)
        {
            localPlayer.ActivateAllButtons();
            localPlayer.TextCurrebyBoot(playerData.currentBootPlayer);
            if (tableInfo.blindLimit != 0 && playerData.step >= tableInfo.blindLimit && !playerData.IsSeenCard)
            {
                photonView.RPC("SetSeenCardTrue", PhotonTargets.All);
                localPlayer.SeenCardText();
                localPlayer.SetLocalInfoText("blind Limit  4 turns");
            }
        }
        myUI.SetCurrentStep();
    }

    [PunRPC]
    private void StopStep()
    {
        myUI.StopStep();
        if (photonView.isMine)
            localPlayer.DeactivateAllButtons();
    }

    [PunRPC]
    public void SetNewCards(string card, int numCard)
    {
        CardData newCard = JsonUtility.FromJson<CardData>(card);
        playerData.currentCards[numCard] = newCard;
        myUI.SetCard(newCard, numCard, false);   
        if (numCard == 2)
        {
            LastCardInHand();
        }
    }

    [PunRPC]
    public void WinHand(int totalBet)
    {
        myUI.SetWinText();
        playerData.Money += totalBet;
        myUI.WinTextMoney(playerData.Money, totalBet);
        StopStep();
        if (photonView.isMine)
        {
            localPlayer.SetTextMoneyTopBar(playerData.Money.ToString());
            _playerSave.AddExp(totalBet);
            _playerSave.SaveNewMoney(playerData.Money);
        }
    }

    [PunRPC]
    public void StartSideShow(string nameCalledPlayer)
    {
        if (photonView.isMine)
        {
            localPlayer.StarSideShow();
        }
    }
    [PunRPC]
    public void ShowCardsOpponet(string namePl)
    {
        if (photonView.isMine)
        {
            FindObjectsOfType<PlayerManagerPun>().First(x => x.playerData.NamePlayer == namePl).ShowCard();
        }
    } 

    public int GetIdOrderUI()
    {
        return myUI.IdOrder;

    }

    public void OnSeenCard()
    {
        if (!playerData.IsSeenCard)
        {
            photonView.RPC("SetSeenCardTrue", PhotonTargets.All);
        }
        else
        {
            photonView.RPC("CallSideShow", PhotonTargets.All);
        }
    }

    public CardData[] GetCurrentCards()
    {
        return playerData.currentCards;
    }

    public string GetNameUI()
    {
        return myUI.name;
    }

    public bool CanIncreaseBoot()
    {
        int newBoot = playerData.currentBootPlayer * 2;
        if (tableInfo.chalLimit == 0 || newBoot <= tableInfo.chalLimit)
        {
            if (playerData.Money >= newBoot)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    private void ShowCard()
    {
        for (int i = 0; i < playerData.currentCards.Length; i++)
        {
            playerData.currentCards[i].isClose = false;
            myUI.SetCard(playerData.currentCards[i], i, false);
        }
        myUI.SetCardCombinationText(playerData.currentCombination);
    }

    [PunRPC]
    public void SetSeenCardTrue()
    {
        playerData.IsSeenCard = true;
        myUI.SetInfoBlind(playerData.IsSeenCard);
        if (photonView.isMine)
        {
            localPlayer.SeenCardText();
            ShowCard();
        }
    }

    public int GetChallLimit()
    {
        return tableInfo.chalLimit;
    } 

    private void LastCardInHand()
    {
        myUI.SetInfoBlind(playerData.IsSeenCard);
        playerData.currentCards = playerData.currentCards.OrderBy(x => x.rankCard).ToArray();
        playerData.IsDoubleBoot = false;
        playerData.step = 0;
        playerData.currentCombination = CardCombination.GetCombinationFromCard(playerData.currentCards);
    }
}
