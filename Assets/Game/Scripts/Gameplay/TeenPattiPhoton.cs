﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[Serializable]
public struct TableInfo
{   
    public int blindLimit, chalLimit, potLimit, startBoot;
}

[Serializable]
public struct ManagerInfo
{
    public int currentPlayerStep, totalPot, currentStake, currentPlayerStepID;
    public bool isStartedGame, isGivingCards;   
    // public string textChat;
    public int playerIdStartedSideShow; 
}

public class TeenPattiPhoton : Photon.PunBehaviour
{
    private DeckManager deckManager;
    private List<PlayerManagerPun> players = new List<PlayerManagerPun>();
    private PlayerUI[] playerUI;

    private const int timeStep = 60;   
    private ManagerInfo managerInfo;
    private TableInfo tableInfo; // need init on start
    public eTable typeTable;
    // private PlayerManagerPun playerStartedSideShow;

    // Use this for initialization
    void Awake()
    {
        typeTable = PlayerSave.singleton.currentTable;
        if (PlayerSave.singleton.currentTable == eTable.Standart || PlayerSave.singleton.currentTable == eTable.Joker)
        {
            tableInfo.startBoot = 100; tableInfo.blindLimit = 4; tableInfo.chalLimit = 12800;  tableInfo.potLimit = 100000;
        }
        else if (PlayerSave.singleton.currentTable == eTable.NoLimit)
        {
            tableInfo.startBoot = 100; tableInfo.blindLimit = 0; tableInfo.chalLimit = 0; tableInfo.potLimit = 0;
        }    
        playerUI = FindObjectsOfType<PlayerUI>();
        deckManager = FindObjectOfType<DeckManager>();
        PhotonNetwork.Instantiate("PlayerConectedPun",Vector3.zero,Quaternion.identity,0);
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {      
    }

    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {      
        base.OnMasterClientSwitched(newMasterClient);     
        players = FindObjectsOfType<PlayerManagerPun>().ToList();
        players = players.OrderBy(x => x.GetIdOrderUI()).ToList();
        bool playerStepDisconect = false;
        if (PhotonNetwork.isMasterClient)
        {          
            Debug.Log("migration on manager");
            for (int i = 0; i < players.Count; i++)
            {
                if (!players[i].isDisconnect)
                {                    
                    playerUI.First(x => x.name == players[i].GetNameUI()).IsFull = true;
                }
                else
                {
                    if (managerInfo.currentPlayerStepID == players[i].photonView.viewID)
                        playerStepDisconect = true;
                    playerUI.First(x => x.name == players[i].GetNameUI()).IsFull = false;
                    players[i].photonView.RPC("ClearFromGame", PhotonTargets.All);
                    players.Remove(players[i]);                   
                    i--;      
                }
            }
            for (int i = 0; i < playerUI.Length; i++)
            {
                if (!playerUI[i].IsFull)
                {
                    playerUI[i].ClearUI();
                }              
            }
            if (managerInfo.isGivingCards)
                StopCoroutine("GiveMovingCards");
            if (CountPlayersInGame() == 1)
            {
                PlayerWinGame(players[GetNumWinPlayer()]);
            }
            else
            {              
                if (managerInfo.isGivingCards)
                {
                    GiveCardsToPlayers();
                }
                else if(playerStepDisconect)
                {
                    NextPlayerStep();
                }
                else
                {
                    Debug.Log("no need change");
                }
            }
        }
    }

    [PunRPC]
    public void SyncRoom(string _managerInfo)
    {
        managerInfo = JsonUtility.FromJson<ManagerInfo>(_managerInfo);      
    }

    public void InitPlayer(PlayerManagerPun connectedPlayer)
    {      
        if (players.Contains(connectedPlayer))
            return;
        players.Add(connectedPlayer);
        List<int> randomFull = new List<int>();
        PlayerUI emptyUI = null;
        for (int i = 0; i <= players.Count; i++)
        {
            int randomUI = UnityEngine.Random.Range(0, playerUI.Length);
            if (randomFull.Contains(randomUI) || playerUI[randomUI].GetIsFull())
                randomFull.Add(randomUI);
            else
            {
                emptyUI = playerUI[randomUI];
                break;
            }
        }
        if (emptyUI == null)
            emptyUI = (playerUI.First(x => x.GetIsFull() == false).GetComponent<PlayerUI>());

        emptyUI.SetIsFull(true);
        connectedPlayer.photonView.RPC("InitTableData", PhotonTargets.All,JsonUtility.ToJson(tableInfo), managerInfo.totalPot);
        connectedPlayer.photonView.RPC("InitUI", PhotonTargets.All, emptyUI.name, JsonUtility.ToJson(connectedPlayer.playerData));
               
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i] != connectedPlayer)
            {                
                players[i].photonView.RPC("InitUI", PhotonTargets.All, players[i].GetNameUI(), JsonUtility.ToJson(players[i].playerData));
            }
        }
        if (!managerInfo.isStartedGame && players.Count >= 2)
        {
            managerInfo.isStartedGame = true;
            Invoke("GiveCardsToPlayers", 3);
        }
    }   


    public void PlayerDisconnected(PlayerManagerPun player)
    {
        if (managerInfo.isGivingCards)
            StopCoroutine("GiveMovingCards");       
        playerUI.First(x => x.name == player.GetNameUI()).SetIsFull(false);
        players.Remove(player);
        player.photonView.RPC("ClearFromGame", PhotonTargets.All);
        if (player.photonView.isMine && PhotonNetwork.isMasterClient && PhotonNetwork.playerList.Length > 1)
        {          
            photonView.RPC("SyncRoom", PhotonTargets.All, JsonUtility.ToJson(managerInfo));         
            PhotonNetwork.SetMasterClient(PhotonNetwork.otherPlayers[0]);
            return;
        }  
        bool playerStepDisconect = false;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].isDisconnect)
            {
                if (managerInfo.currentPlayerStepID == players[i].photonView.viewID)
                    playerStepDisconect = true;
            }
        }
        if (CountPlayersInGame() == 1)
        {
            PlayerWinGame(players[GetNumWinPlayer()]);
        }
        else
        {
            if (managerInfo.isGivingCards)
            {
                GiveCardsToPlayers();
            }
            else if (playerStepDisconect)
            {
                NextPlayerStep();
            }
            else
            {
                Debug.Log("no need change");
            }
        }
    }

    public void Chaal(PlayerManagerPun player)
    {
        if (player.playerData.IsDoubleBoot)
        {
            managerInfo.currentStake *= 2;
        }
        int calculateBoot = CalculatePlayersBoot(player);
        if (player.playerData.Money < calculateBoot)
        {
            Debug.Log("not money = packed");
            player.photonView.RPC("PackPlayerByServer", PhotonTargets.All);
            NextPlayerStep();
        }
        else
        {
            MoneyPlayerToTable(player, calculateBoot);
            if (tableInfo.potLimit  == 0 ||  managerInfo.totalPot < tableInfo.potLimit)
            {
                NextPlayerStep();
            }
            else
            {
                StartCoroutine("RisePotLimit");
            }
        }
    }

    public void PackPlayerFromClient()
    {
        Invoke("CheckOnWin", 2);
    }

    public void StartSideShow(PlayerManagerPun firstPlayer)
    {
        for (int i=0;i<players.Count;i++)
        {
            if (players[i] == firstPlayer)
                managerInfo.playerIdStartedSideShow = i;
        }
       // playerStartedSideShow = firstPlayer;
        StartCoroutine("SideShowCorotine", firstPlayer);
    }

    public void AcceptSideShow(PlayerManagerPun callPlayer)
    {
        StartCoroutine("AcceptShowCorotine", callPlayer);
    }

    public void DeclineSideShow(PlayerManagerPun firstPlayer)
    {
        foreach (var item in players)
        {
            item.photonView.RPC("GlobalInfo", PhotonTargets.All, firstPlayer.playerData.NamePlayer + " rejected side show");
        }
        MoneyPlayerToTable(firstPlayer, managerInfo.currentStake);
        NextPlayerStep();
    }

    public void AddChat(string _textAdd,int uiOrder)
    {      
       // managerInfo.textChat = managerInfo.textChat + "\r\n" + _textAdd;
        foreach (var item in players)
        {
            //item.photonView.RPC("NewChat", PhotonTargets.All, managerInfo.textChat);
            item.photonView.RPC("NewChat", PhotonTargets.All, _textAdd, uiOrder);
        }
    }

    private int CalculatePlayersBoot(PlayerManagerPun playerData)
    {
        int bootForPlayer = managerInfo.currentStake;
        bool previousSeen = FindPreviousPlayer().playerData.IsSeenCard;
        bool currentSeen = playerData.playerData.IsSeenCard;
        if (!currentSeen && previousSeen)
            bootForPlayer /= 2;
        else if (currentSeen && !previousSeen)
            bootForPlayer *= 2;
        return bootForPlayer;
    }

    private PlayerManagerPun FindPreviousPlayer()
    {
        int previousPlayer = managerInfo.currentPlayerStep;
        for (int i = 0; i < players.Count; i++)
        {
            previousPlayer--;
            if (previousPlayer < 0)
                previousPlayer = players.Count - 1;
            if (players[previousPlayer].playerData.playerType == ePlayerType.PlayerStartGame && !players[previousPlayer].playerData.IsPacked)
            {
                return players[previousPlayer];
            }
        }
        return null;
    }

    private void GiveCardsToPlayers()
    {
        players = players.OrderBy(x => x.GetIdOrderUI()).ToList();
        for (int i = 0; i < players.Count; i++)
        {
            players[i].photonView.RPC("SetInGame", PhotonTargets.All);
        }
        if (players.Count < 2)
        {
            managerInfo.isStartedGame = false;
            return;
        }
        managerInfo.isStartedGame = true;
        managerInfo.currentStake = tableInfo.startBoot;
        deckManager.NewDeck();
        StopCoroutine("GiveMovingCards");
        StartCoroutine("GiveMovingCards");
    }

    private void PlayerDontMakeStep()
    {
        players[managerInfo.currentPlayerStep].photonView.RPC("PackPlayerByServer", PhotonTargets.All);      
    }

    private void NextPlayerStep()
    {
        if (CountPlayersInGame() == 1)
        {
            PlayerWinGame(players[GetNumWinPlayer()]);
            return;
        }
        bool findPlayer = false;
        for (int i = 0; i < players.Count; i++)
        {
            managerInfo.currentPlayerStep++;
            if (managerInfo.currentPlayerStep >= players.Count)
                managerInfo.currentPlayerStep = 0;
            if (players[managerInfo.currentPlayerStep].playerData.playerType == ePlayerType.PlayerStartGame && !players[managerInfo.currentPlayerStep].playerData.IsPacked)
            {             
                players[managerInfo.currentPlayerStep].photonView.RPC("StartStep", PhotonTargets.All, managerInfo.currentStake);
                managerInfo.currentPlayerStepID = players[managerInfo.currentPlayerStep].photonView.viewID;
                findPlayer = true;
                break;
            }
        }
        if (!findPlayer && players.Count > 1)
            PlayerWinGame(players[GetNumWinPlayer()]);
    }

    private void CheckOnWin()
    {
        if (CountPlayersInGame() == 1)
        {
            PlayerWinGame(players[GetNumWinPlayer()]);
        }
        else
        {
            NextPlayerStep();
        }
    }

    private void PlayerWinGame(PlayerManagerPun playerWin)
    {
        playerWin.photonView.RPC("WinHand", PhotonTargets.All, managerInfo.totalPot);
        managerInfo.totalPot = 0;
        foreach (var item in players)
        {
            item.photonView.RPC("SetTotal", PhotonTargets.All, managerInfo.totalPot);
        }
        if (players.Count >= 2)
        {
            managerInfo.isStartedGame = true;
            Invoke("GiveCardsToPlayers", 5);
        }
        else
        {
            managerInfo.isStartedGame = false;
        }
    }

    private void MoneyPlayerToTable(PlayerManagerPun playerGiveMoney, int calculatedBoot)
    {
        playerGiveMoney.photonView.RPC("GiveMoney", PhotonTargets.All, calculatedBoot);
        managerInfo.totalPot += calculatedBoot;
        foreach (var item in players)
        {
            item.photonView.RPC("SetTotal", PhotonTargets.All, managerInfo.totalPot);
        }
    }

    private int GetNumWinPlayer()
    {
        int numPlayer = 0;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].playerData.playerType == ePlayerType.PlayerStartGame && !players[i].playerData.IsPacked)
            {
                numPlayer = i;
            }
        }
        return numPlayer;
    }

    private int CountPlayersInGame()
    {
        int playersInGame = 0;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i] == null)
            {
                players.Remove(players[i]);
                i--;
                continue;
            }
              
            if (players[i].playerData.playerType == ePlayerType.PlayerStartGame && !players[i].playerData.IsPacked)
            {
                playersInGame++;
            }
        }
        return playersInGame;
    }

    private IEnumerator RisePotLimit()
    {
        foreach (var item in players)
        {
            item.photonView.RPC("GlobalInfo", PhotonTargets.All, " pot limit");
        }
        foreach (var item in players)
        {
            if (item.playerData.playerType == ePlayerType.PlayerStartGame && !item.playerData.IsPacked)
            {
                item.photonView.RPC("ShowCardForAll", PhotonTargets.All);
                yield return new WaitForSeconds(1f);
            }
        }
        yield return new WaitForSeconds(1f);
        PlayerWinGame(CardCombination.FindWinnerFromAll(players.ToArray()));
    }

    private IEnumerator AcceptShowCorotine(PlayerManagerPun callPlayer)
    {
        foreach (var item in players)
        {
            item.photonView.RPC("GlobalInfo", PhotonTargets.All, callPlayer.playerData.NamePlayer + " accept side show");
        }
        callPlayer.photonView.RPC("SetSeenCardTrue", PhotonTargets.All);
        PlayerManagerPun playerStartedSideShow = players[managerInfo.playerIdStartedSideShow];     
        callPlayer.photonView.RPC("ShowCardsOpponet", PhotonTargets.All, playerStartedSideShow.playerData.NamePlayer);
        playerStartedSideShow.photonView.RPC("ShowCardsOpponet", PhotonTargets.All, callPlayer.playerData.NamePlayer);       
        yield return new WaitForSeconds(3f);
        bool firstPlayerWin = CardCombination.CompareCards(playerStartedSideShow.GetCurrentCards(), callPlayer.GetCurrentCards());
        if (!firstPlayerWin)
        {
            playerStartedSideShow.photonView.RPC("PackPlayerByServer", PhotonTargets.All);
            foreach (var item in players)
            {
                item.photonView.RPC("GlobalInfo", PhotonTargets.All, callPlayer.playerData.NamePlayer + " win side show");              
            }
        }
        else
        {
            callPlayer.photonView.RPC("PackPlayerByServer", PhotonTargets.All);
            foreach (var item in players)
            {
                item.photonView.RPC("GlobalInfo", PhotonTargets.All, playerStartedSideShow.playerData.NamePlayer + " win side show");            
            }
        }
        yield return new WaitForSeconds(2f);
        NextPlayerStep();
    }

    private IEnumerator SideShowCorotine(PlayerManagerPun firstPlayer)
    {
        MoneyPlayerToTable(firstPlayer, managerInfo.currentStake);
        PlayerManagerPun callPreviousPlayer = FindPreviousPlayer();
        if (CountPlayersInGame() == 2)
        {
            foreach (var item in players)
            {                
                item.photonView.RPC("ShowCardForAll", PhotonTargets.All);
                item.photonView.RPC("GlobalInfo", PhotonTargets.All, firstPlayer.playerData.NamePlayer + " call compromise with " + callPreviousPlayer.playerData.NamePlayer);
            }
            yield return new WaitForSeconds(4f);
            bool firstPlayerWin = CardCombination.CompareCards(firstPlayer.GetCurrentCards(), callPreviousPlayer.GetCurrentCards());
            if (!firstPlayerWin)
            {
                foreach (var item in players)
                {
                    item.photonView.RPC("GlobalInfo", PhotonTargets.All, callPreviousPlayer.playerData.NamePlayer + " win hand");
                }
                firstPlayer.photonView.RPC("PackPlayerByServer", PhotonTargets.All);
                PlayerWinGame(callPreviousPlayer);
            }
            else
            {
                foreach (var item in players)
                {
                    item.photonView.RPC("GlobalInfo", PhotonTargets.All, firstPlayer.playerData.NamePlayer + " win hand");
                }
                callPreviousPlayer.photonView.RPC("PackPlayerByServer", PhotonTargets.All);
                PlayerWinGame(firstPlayer);
            }
        }
        else
        {
            foreach (var item in players)
            {
                item.photonView.RPC("GlobalInfo", PhotonTargets.All, firstPlayer.playerData.NamePlayer + " side show with " + callPreviousPlayer.playerData.NamePlayer);
            }
            callPreviousPlayer.photonView.RPC("StartSideShow", PhotonTargets.All, firstPlayer.playerData.NamePlayer);
        }
    }

    private IEnumerator GiveMovingCards()
    {
        managerInfo.isGivingCards = true;
        yield return new WaitForSeconds(1);
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].playerData.playerType == ePlayerType.PlayerStartGame) 
                MoneyPlayerToTable(players[i], managerInfo.currentStake);            
        }
        yield return new WaitForSeconds(1);
        for (int step = 0; step < 3; step++)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].playerData.playerType == ePlayerType.PlayerStartGame)
                {
                    yield return new WaitForSeconds(0.2f);
                    CardData randomCard = deckManager.GetRandomCard();                   
                    if (typeTable == eTable.Joker && step == 2)
                    {
                        randomCard.suitCard = eCardSuit.Joker;
                        randomCard.rankCard = 15;
                    }                       
                    players[i].photonView.RPC("SetNewCards", PhotonTargets.All, JsonUtility.ToJson(randomCard), step); 
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
        managerInfo.currentPlayerStep = UnityEngine.Random.Range(-1, players.Count);
        managerInfo.isGivingCards = false;
        NextPlayerStep();
    }


}
