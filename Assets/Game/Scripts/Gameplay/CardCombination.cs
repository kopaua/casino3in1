﻿public static class CardCombination
{
    public static eCombination GetCombinationFromCard(CardData[] cardsPlayer)
    {
        eCombination currentCombination = eCombination.HighCard;
        if (cardsPlayer[2].suitCard != eCardSuit.Joker)
        {
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard || cardsPlayer[1].rankCard == cardsPlayer[2].rankCard || cardsPlayer[0].rankCard == cardsPlayer[2].rankCard)
                currentCombination = eCombination.Pair;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard && cardsPlayer[1].rankCard == cardsPlayer[2].rankCard && cardsPlayer[0].rankCard == cardsPlayer[2].rankCard)
                currentCombination = eCombination.Trail;
            if (cardsPlayer[0].suitCard == cardsPlayer[1].suitCard && cardsPlayer[1].suitCard == cardsPlayer[2].suitCard && cardsPlayer[0].suitCard == cardsPlayer[2].suitCard)
                currentCombination = eCombination.Flush;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard + 1 && cardsPlayer[0].rankCard == cardsPlayer[2].rankCard + 2)
                currentCombination = eCombination.Sequence;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard - 1 && cardsPlayer[0].rankCard == cardsPlayer[2].rankCard - 2)
                currentCombination = eCombination.Sequence;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard - 1 && cardsPlayer[0].rankCard == cardsPlayer[2].rankCard - 2 &&
                cardsPlayer[0].suitCard == cardsPlayer[1].suitCard && cardsPlayer[1].suitCard == cardsPlayer[2].suitCard && cardsPlayer[0].suitCard == cardsPlayer[2].suitCard)
                currentCombination = eCombination.StraightFlush;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard + 1 && cardsPlayer[0].rankCard == cardsPlayer[2].rankCard + 2 &&
               cardsPlayer[0].suitCard == cardsPlayer[1].suitCard && cardsPlayer[1].suitCard == cardsPlayer[2].suitCard && cardsPlayer[0].suitCard == cardsPlayer[2].suitCard)
                currentCombination = eCombination.StraightFlush;
        }
        else
        {           
            currentCombination = eCombination.Pair;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard)
                currentCombination = eCombination.Trail;
            if (cardsPlayer[0].suitCard == cardsPlayer[1].suitCard)
                currentCombination = eCombination.Flush;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard + 1)
                currentCombination = eCombination.Sequence;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard - 1)
                currentCombination = eCombination.Sequence;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard - 1  &&  cardsPlayer[0].suitCard == cardsPlayer[1].suitCard)
                currentCombination = eCombination.StraightFlush;
            if (cardsPlayer[0].rankCard == cardsPlayer[1].rankCard + 1 &&  cardsPlayer[0].suitCard == cardsPlayer[1].suitCard )
                currentCombination = eCombination.StraightFlush;
        }
        return currentCombination;
    }

    public static int GetHighCard(CardData[] cards)
    {
        int numCard = 0;
        for (int i=0;i< cards.Length;i++)
        {
            if (cards[i].rankCard > numCard)
                numCard = cards[i].rankCard;
        }
        return numCard;
    }

    public static PlayerManagerPun FindWinnerFromAll(PlayerManagerPun[] players)
    {
        eCombination[] playerInGameCombination = new eCombination[players.Length];
        int[] playerInGameHigh = new int[players.Length];

        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].playerData.playerType != ePlayerType.Empty && !players[i].playerData.IsPacked)
            {
                playerInGameCombination[i] = GetCombinationFromCard(players[i].GetCurrentCards());
                playerInGameHigh[i] = GetHighCard(players[i].GetCurrentCards());
            }
        }

        int maxCombinaion = 0;
        int numWinPlayer = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (maxCombinaion < (int)playerInGameCombination[i])
            {
                maxCombinaion = (int)playerInGameCombination[i];
                numWinPlayer = i;
            }
            else if (maxCombinaion == (int)playerInGameCombination[i])
            {
                if (GetHighCard(players[numWinPlayer].GetCurrentCards()) < playerInGameHigh[i])
                {
                    maxCombinaion = (int)playerInGameCombination[i];
                    numWinPlayer = i;
                }
            }
        }
        return players[numWinPlayer];
    }

    public static PlayerManager FindWinnerFromAll(PlayerManager[] players)
    {
        eCombination[] playerInGameCombination = new eCombination[players.Length];
        int[] playerInGameHigh = new int[players.Length];

        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetPlayerType() != ePlayerType.Empty && !players[i].GetIsPacked())
            {
                playerInGameCombination[i] = GetCombinationFromCard(players[i].GetCurrentCards());
                playerInGameHigh[i] = GetHighCard(players[i].GetCurrentCards());
            }
        }
      
        int maxCombinaion = 0;
        int numWinPlayer = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if(maxCombinaion < (int)playerInGameCombination[i])
            {
                maxCombinaion = (int)playerInGameCombination[i];
                numWinPlayer =i;
            }else if (maxCombinaion == (int)playerInGameCombination[i])
            {
                if (GetHighCard(players[numWinPlayer].GetCurrentCards()) < playerInGameHigh[i])
                {
                    maxCombinaion = (int)playerInGameCombination[i];
                    numWinPlayer = i;
                }                 
            }
        }
        return players[numWinPlayer];
    }

   public static bool CompareCards(CardData[] cardsPlayer1, CardData[] cardsPlayer2)
    {
        eCombination player1 = GetCombinationFromCard(cardsPlayer1);
        eCombination player2 = GetCombinationFromCard(cardsPlayer2);
        if((int)player1 > (int)player2)
        {
            return true;
        }
        else if((int)player1 == (int)player2)
        {           
            int highCard1 = GetHighCard(cardsPlayer1);
            int highCard2 = GetHighCard(cardsPlayer2);
            if (highCard1 > highCard2)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
        
    }
}
