﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class TeenPattiManager : NetworkBehaviour
{
    private DeckManager deckManager;
    private List<PlayerManager> players = new List<PlayerManager>();
    private PlayerUI[] playerUI;

    private const int timeStep = 60;

    [SyncVar]
    private int currentPlayerStep;      
    [SyncVar]
    private int totalPot, currentStake;   
    [SyncVar]
    private bool isStartedGame,isGivingCards;
    [SyncVar]
    private string textChat;

    private PlayerManager playerStartedSideShow;
    private int blindLimit, chalLimit, potLimit, startBoot; // need init on start

    // Use this for initialization
    void Awake()
    {
        startBoot = 100;
        blindLimit = 4;
        chalLimit = 12800;
        potLimit = 100000;
        playerUI = FindObjectsOfType<PlayerUI>();
        deckManager = FindObjectOfType<DeckManager>();
    }   

    public void ReconnectHost()
    {
        if (isServer && hasAuthority)
            Invoke("ReconnectHostDelay", 1);
        else
            CmdReconnectHostDelay();         
    }

    [Command]
    private void CmdReconnectHostDelay()
    {
        Debug.Log("CmdReconnectHostDelay");
        ReconnectHostDelay();
    }

    private void ReconnectHostDelay()
    {
        Debug.Log("ReconnectHostDelay");
        players = FindObjectsOfType<PlayerManager>().ToList();
        List<string> filledui = new List<string>();
        for (int i = 0; i < players.Count; i++)
        {
            filledui.Add(players[i].GetNameUI());
        }
        for (int i = 0; i < playerUI.Length; i++)
        {
            if (filledui.Contains(playerUI[i].name))
                playerUI[i].SetIsFull(true);
            else
            {
                playerUI[i].ClearUI();
                RpcCleraUI(playerUI[i].name);
            }
        }
        if (isServer && hasAuthority)
            NextPlayerStep();
    }  

    [ClientRpc]
    public void RpcCleraUI(string nameUI)
    {
        for (int i = 0; i < playerUI.Length; i++)
        {
          if  (playerUI[i].name == nameUI)
               playerUI[i].ClearUI();
        }
    }

    public void InitPlayer(PlayerManager connectedPlayer)
    {        
        Debug.Log("Check InitPlayer");
        if (players.Contains(connectedPlayer))
            return;
        Debug.Log("OK InitPlayer");
        players.Add(connectedPlayer);
        List<int> randomFull = new List<int>();
        PlayerUI emptyUI = null;
        for (int i = 0; i <= 25; i++)
        {
            int randomUI = Random.Range(0, playerUI.Length);
            if (randomFull.Contains(randomUI) || playerUI[randomUI].GetIsFull())            
                randomFull.Add(randomUI);            
            else
            {
                emptyUI = playerUI[randomUI];
                break;
            }                   
        }
        if (emptyUI == null)
            emptyUI = (playerUI.First(x => x.GetIsFull() == false).GetComponent<PlayerUI>());

        emptyUI.SetIsFull(true);
        connectedPlayer.RpcInitPlayerData(emptyUI.name,  startBoot, blindLimit, chalLimit, potLimit);
        if (!isStartedGame && players.Count >= 2)
        {
            isStartedGame = true;
            Invoke("GiveCardsToPlayers", 3);
        }
    }

    public void SyncGame(PlayerManager newPlayer)
    {    
        for(int i =0; i< players.Count;i++)
        {
            players[i].RpcInitUI(players[i].GetNameUI());
        }        
    }

    private void StopHostDelay()
    {
        NetworkManager.singleton.StopHost();
    }

    public void PlayerDisconnected(PlayerManager player)
    {
        if (isGivingCards)        
            StopCoroutine("GiveMovingCards");
        playerUI.First(x => x.name == player.GetNameUI()).SetIsFull(false);
        players.Remove(player);
        player.CmdClearPlayerFromGame();
        if (player.isServer && player.hasAuthority)
        {
            Invoke("StopHostDelay",1);          
        }
        if (CountPlayersInGame() == 1)
        {
            PlayerWinGame(players[GetNumWinPlayer()]);
        }
        else
        {
            if (isGivingCards)
            {
                GiveCardsToPlayers();
            }
            else
            {
                NextPlayerStep();
            }
        }
    }       

    public void Chaal(PlayerManager player)
    {
      
        if (player.GetIsDouble())
        {
            currentStake *= 2;
        }
        int calculateBoot = CalculatePlayersBoot(player);
        if (player.GetMoney() < calculateBoot)
        {
            Debug.Log("not money = packed");
            player.RpcSetPacked();
            PackPlayer();
        }
        else
        {
            MoneyPlayerToTable(player, calculateBoot);
            if (totalPot < potLimit)
            {
                NextPlayerStep();
            }
            else
            {
                StartCoroutine("RisePotLimit");
            }
        }           
    }

    public void PackPlayer()
    {
        Invoke("CheckOnWin", 2);                        
    }

    public void StartSideShow(PlayerManager firstPlayer)
    {
        playerStartedSideShow = firstPlayer;
        StartCoroutine("SideShowCorotine", firstPlayer);        
    }

    public void AcceptSideShow(PlayerManager callPlayer)
    {
        StartCoroutine("AcceptShowCorotine",callPlayer);    
    }

    public void DeclineSideShow(PlayerManager firstPlayer)
    {
        foreach (var item in players)
        {
            item.RpcGlobalInfo(firstPlayer.playerData.NamePlayer + " rejected side show");
        }
        MoneyPlayerToTable(firstPlayer, currentStake); 
        NextPlayerStep();
    }

    public void AddChat(string _textAdd)
    {
        textChat = textChat + "\r\n" + _textAdd;
        foreach (var item in players)
        {
            item.RpcNewChat(textChat);
        }
    }

    private int CalculatePlayersBoot(PlayerManager playerData)
    {
        int bootForPlayer = currentStake; 
        bool previousSeen = FindPreviousPlayer().GetIsSeenCard();
        bool currentSeen = playerData.GetIsSeenCard();
        if (!currentSeen && previousSeen)
            bootForPlayer /= 2;
        else if (currentSeen && !previousSeen)
            bootForPlayer *= 2;
        return bootForPlayer;
    }

    private PlayerManager FindPreviousPlayer()
    {
        int previousPlayer = currentPlayerStep;
        for (int i = 0; i < players.Count; i++)
        {       
            previousPlayer--;
            if (previousPlayer < 0)
                previousPlayer = players.Count-1;         
            if (players[previousPlayer].GetPlayerType() == ePlayerType.PlayerStartGame && !players[previousPlayer].GetIsPacked())
            {
               return  players[previousPlayer];              
            }
        }
        return null;
    }

    private void GiveCardsToPlayers()
    {
        players = players.OrderBy(x => x.GetIdOrderUI()).ToList();
        for (int i = 0; i < players.Count; i++)
        {
            players[i].RpcSetInGame();
        }
        if (players.Count < 2)
        {
            isStartedGame = false;
            return;
        }
        isStartedGame = true;
        currentStake = startBoot;
        deckManager.NewDeck();      
        StartCoroutine("GiveMovingCards");
    }  

    private void PlayerDontMakeStep()
    {
        players[currentPlayerStep].RpcSetPacked();
        PackPlayer();
    }

    private void NextPlayerStep()
    {
        if (CountPlayersInGame() == 1)
        {
            PlayerWinGame(players[GetNumWinPlayer()]);
            return;
        }
        bool findPlayer = false;             
        for (int i =0;i< players.Count; i++)
        {
            currentPlayerStep++;
            if (currentPlayerStep >= players.Count )
                currentPlayerStep = 0;
            if (players[currentPlayerStep].GetPlayerType() == ePlayerType.PlayerStartGame && !players[currentPlayerStep].GetIsPacked())
            {
                players[currentPlayerStep].RpcSetNewBoot(currentStake);
                players[currentPlayerStep].RpcStartStep();               
                findPlayer = true;              
                break;
            }
        }
        if (!findPlayer && players.Count > 1)
            PlayerWinGame(players[GetNumWinPlayer()]);
    }   

    private void  CheckOnWin()
    {   
        if (CountPlayersInGame() == 1)
        {          
            PlayerWinGame(players[GetNumWinPlayer()]);          
        }
        else
        {
            NextPlayerStep();
        }       
    }

    private void PlayerWinGame(PlayerManager playerWin)
    {             
        playerWin.RpcWinHand(totalPot);
        totalPot = 0;
        foreach (var item in players)
        {
            item.RpcSetTotal(totalPot);
        }
        if (players.Count >= 2)
        {
            isStartedGame = true;
            Invoke("GiveCardsToPlayers", 5);
        }
        else
        {           
            isStartedGame = false;
        }
    }    

    private void MoneyPlayerToTable(PlayerManager playerGiveMoney, int calculatedBoot)
    {      
        playerGiveMoney.RpcGiveMoney(calculatedBoot);
        totalPot += calculatedBoot;
        foreach (var item in players)
        {
            item.RpcSetTotal(totalPot);
        }       
    }

    private int GetNumWinPlayer()
    {
        int numPlayer = 0;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].GetPlayerType() == ePlayerType.PlayerStartGame && !players[i].GetIsPacked())
            {
                numPlayer = i;
            }
        }
        return numPlayer;
    }

    private int CountPlayersInGame()
    {
        int playersInGame = 0;
        for (int i = 0; i < players.Count; i++)
        {           
            if (players[i].GetPlayerType() == ePlayerType.PlayerStartGame && !players[i].GetIsPacked())
            {
                playersInGame++;
            }
        }
        return playersInGame;
    }

    private IEnumerator RisePotLimit()
    {
        foreach (var item in players)
        {
            item.RpcGlobalInfo(" pot limit");
        }  
        foreach (var item in players)
        {          
            if(item.GetPlayerType() == ePlayerType.PlayerStartGame && !item.GetIsPacked())
            {
                item.RpcShowCardForAll(); 
                yield return new WaitForSeconds(1f);
            }             
        }         
         yield return new WaitForSeconds(1f);
        PlayerWinGame(CardCombination.FindWinnerFromAll(players.ToArray()));
    }

    private IEnumerator AcceptShowCorotine(PlayerManager callPlayer)
    {      
        foreach (var item in players)
        {
            item.RpcGlobalInfo(callPlayer.playerData.NamePlayer + " accept side show");
        }
        callPlayer.RpcSetSeenCardTrue();
        callPlayer.RpcShowCardsOpponet(playerStartedSideShow.playerData.NamePlayer);
        playerStartedSideShow.RpcShowCardsOpponet(callPlayer.playerData.NamePlayer);
        yield return new WaitForSeconds(3f);
        bool firstPlayerWin = CardCombination.CompareCards(playerStartedSideShow.GetCurrentCards(), callPlayer.GetCurrentCards());
        if (!firstPlayerWin)
        {
            playerStartedSideShow.RpcSetPacked();
            foreach (var item in players)
            {
                item.RpcGlobalInfo(callPlayer.playerData.NamePlayer + " win side show");
            }
        }
        else
        {
            callPlayer.RpcSetPacked();
            foreach (var item in players)
            {
                item.RpcGlobalInfo(playerStartedSideShow.playerData.NamePlayer + " win side show");
            }
        }
        yield return new WaitForSeconds(2f);
        NextPlayerStep();
    }

    private IEnumerator SideShowCorotine(PlayerManager firstPlayer)
    {  
        MoneyPlayerToTable(firstPlayer, currentStake);
        PlayerManager callPreviousPlayer = FindPreviousPlayer();
        if (CountPlayersInGame() == 2)
        { 
            foreach (var item in players)
            {              
                item.RpcShowCardForAll();
                item.RpcGlobalInfo(firstPlayer.playerData.NamePlayer + " call compromise with " + callPreviousPlayer.playerData.NamePlayer);
            }        
            yield return new WaitForSeconds(4f);
            bool firstPlayerWin = CardCombination.CompareCards(firstPlayer.GetCurrentCards(), callPreviousPlayer.GetCurrentCards());
            if (!firstPlayerWin)
            {
                foreach (var item in players)
                {
                    item.RpcGlobalInfo(callPreviousPlayer.playerData.NamePlayer + " win hand");
                }
                firstPlayer.RpcSetPacked();               
                PlayerWinGame(callPreviousPlayer);
            }
            else
            {
                foreach (var item in players)
                {
                    item.RpcGlobalInfo(firstPlayer.playerData.NamePlayer + " win hand");
                }
                callPreviousPlayer.RpcSetPacked();                           
                PlayerWinGame(firstPlayer);
            }
        }
        else
        {           
            foreach (var item in players)
            {              
                item.RpcGlobalInfo(firstPlayer.playerData.NamePlayer + " side show with " + callPreviousPlayer.playerData.NamePlayer);
            }
            callPreviousPlayer.RpcStartSideShow(firstPlayer.playerData.NamePlayer);
        }             
    }

    private IEnumerator GiveMovingCards()
    {
        isGivingCards = true;
           yield return new WaitForSeconds(1);
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].GetPlayerType() == ePlayerType.PlayerStartGame)
            {
                players[i].RpcReloadPlayerToNewGame();
                MoneyPlayerToTable(players[i],currentStake);
            }
        }
        yield return new WaitForSeconds(1);
        for (int step = 0; step < 3; step++)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].GetPlayerType() == ePlayerType.PlayerStartGame)
                {
                    yield return new WaitForSeconds(0.2f);
                    CardData randomCard = deckManager.GetRandomCard();
                  
                    players[i].RpcSetNewCards(JsonUtility.ToJson(randomCard), step);                   
                }               
            }
            yield return new WaitForSeconds(0.2f);
        }
        currentPlayerStep = Random.Range(-1, players.Count);
        isGivingCards = false;
        NextPlayerStep();
    }


}
