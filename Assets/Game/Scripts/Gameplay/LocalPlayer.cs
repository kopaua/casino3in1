﻿using UnityEngine;
using UnityEngine.UI;

public class LocalPlayer : MonoBehaviour
{
    private Button homeButton;
    private Button ShowButton;
    private Button ChaalButton;
    private Button PackButton;
    private Button IncreaseBet, DecreaseBet;
    private Button AcceptSide, DeclineSide;
    private Button ChatButton;
    private Text textLocalInfo, textCurrentBoot,textChat,textMoney;
    private GameObject PanelAcceptSideShow, PanelChat;
    private InputField inputChat;
    private PlayerManager playerManager;

    private bool isButtonDown;
    // Use this for initialization
    protected void  Start ()
    {              
        Transform gui = FindObjectOfType<TeenPatiHUD>().transform.Find("PlayerLocalPanel");
        ShowButton = gui.Find("ButtonShow").GetComponent<Button>();
        ShowButton.onClick.AddListener(OnSeenCard);
        ChaalButton = gui.Find("ButtonChall").GetComponent<Button>();
        ChaalButton.onClick.AddListener(OnChaal);
        IncreaseBet = gui.Find("ButtonAdd").GetComponent<Button>();
        IncreaseBet.onClick.AddListener(OnIncreaseBet);
        DecreaseBet = gui.Find("ButtonLess").GetComponent<Button>();
        DecreaseBet.onClick.AddListener(OnDecreaseBet);
        PackButton = gui.Find("ButtonPack").GetComponent<Button>();
        ChatButton = gui.Find("ButtonChat").GetComponent<Button>();
        ChatButton.onClick.AddListener(OnChatButton);        
        PanelChat = gui.Find("PanelChat").gameObject;      
        inputChat = gui.Find("PanelChat/InputField").GetComponent<InputField>();
        inputChat.onEndEdit.AddListener(OnEndChat);
        textChat = gui.Find("PanelChat/Scroll View/Viewport/TextChat").GetComponent<Text>();
        PanelChat.GetComponent<CanvasGroup>().alpha = 1;
        PanelChat.SetActive(false);
        PackButton.onClick.AddListener(OnPack);
        textCurrentBoot =  gui.Find("TextCurrentBoot").GetComponent<Text>();
        textLocalInfo = gui.Find("TextLocalInfo").GetComponent<Text>();
        PanelAcceptSideShow = gui.Find("PanelAcceptSideShow").gameObject;
        AcceptSide = PanelAcceptSideShow.transform.Find("ButtonYes").GetComponent <Button>();
        AcceptSide.onClick.AddListener(OnAcceptSideShow);
        DeclineSide = PanelAcceptSideShow.transform.Find("ButtonNo").GetComponent<Button>();
        DeclineSide.onClick.AddListener(OnDeclineSideShow);
        homeButton = GameObject.Find("HomeButton").GetComponent<Button>();
        homeButton.onClick.AddListener(OnHomeButton);
        PanelAcceptSideShow.SetActive(false);
        PanelAcceptSideShow.GetComponent<CanvasGroup>().alpha = 1;
        playerManager = GetComponent<PlayerManager>();
        textMoney = GameObject.Find("CoinBarPlayer/Text").GetComponent<Text>();
        DeactivateAllButtons();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Home) || Input.GetKeyDown(KeyCode.Escape))
        {
            OnHomeButton();           
        }
    }

    public void SetTextMoneyTopBar(string _text)
    {
        textMoney.text = _text;
    }

    public void NewChatText(string _text)
    {
        PanelChat.SetActive(true);
        textChat.text = _text;
    }

    public void StarSideShow()
    {
        SetLocalInfoText("start side show");
        PanelAcceptSideShow.SetActive(true);
    }

    public void OnAcceptSideShow()
    {
        PanelAcceptSideShow.SetActive(false);
        playerManager.CmdAcceptSideShow();
    }

    public void OnDeclineSideShow()
    {
        PanelAcceptSideShow.SetActive(false);
        playerManager.CmdDeclineSideShow();
    }

    public void OnSeenCard()
    {       
        playerManager.SeenCard();
        SeenCardText();
    }  

    public void SeenCardText()
    {
        if (playerManager.GetIsSeenCard())
            ShowButton.GetComponentInChildren<Text>().text = "Side Show";
       
    }

    public void TextShowButtonToSeen()
    {
        ShowButton.GetComponentInChildren<Text>().text = "Seen";
    }
    
    public void TextCurrebyBoot(int _boot)
    {
        textCurrentBoot.text = _boot.ToString();
    }   

    public void OnDecreaseBet()
    {
        IncreaseBet.interactable = true;
        DecreaseBet.interactable = false;
        textCurrentBoot.text = playerManager.GetCurrentBoot().ToString();
        playerManager.CmdDecreaseBet();
    }    

    public void OnIncreaseBet()
    {
        if (playerManager.CanIncreaseBoot())
        {
            IncreaseBet.interactable = false;
            DecreaseBet.interactable = true;
            textCurrentBoot.text = (playerManager.GetCurrentBoot() * 2).ToString();
            playerManager.CmdIncreaseBet();
        }
        else
        {
            IncreaseBet.interactable = false;
            DecreaseBet.interactable = false;
        }                
    }

    public void OnPack()
    {
        DeactivateAllButtons();
        playerManager.CmdPack();
    }    

    public void OnChaal()
    {
        DeactivateAllButtons();
        playerManager.CmdChaal();       
    }   

    public void SetLocalInfoText(string _text)
    {
        textLocalInfo.enabled = true;
        textLocalInfo.text = _text;
        Invoke("DeactivateInfoText",3);
    }

    public void DeactivateInfoText()
    {
        textLocalInfo.enabled = false;
    }

    public void ActivateAllButtons()
    {
        ShowButton.interactable = true;
        ChaalButton.interactable = true;
        if (playerManager.playerData.currentBootPlayer < playerManager.chalLimit)
          IncreaseBet.interactable = true;
        else
          IncreaseBet.interactable = false;
        DecreaseBet.interactable = false;
        PackButton.interactable = true;
        if (playerManager.playerData.Money < playerManager.playerData.currentBootPlayer)
        {
            DeactivateAllButtons();
            PackButton.interactable = true;
        }
    }

    public void DeactivateAllButtons()
    {       
        ShowButton.interactable = false;
        ChaalButton.interactable = false;
        IncreaseBet.interactable = false;
        DecreaseBet.interactable = false;
        PackButton.interactable = false;
    }

    private void OnEndChat(string _chatText)
    {
        playerManager.CmdEndTypeChat(playerManager.playerData.NamePlayer+": "+ _chatText);
    }

    private void OnChatButton()
    {
        if (PanelChat.activeInHierarchy)
            PanelChat.SetActive(false);
        else
            PanelChat.SetActive(true);
    }

    private void OnHomeButton()
    {
        if (!isButtonDown)
        {         
            playerManager.CmdDisconnect();
            isButtonDown = true;
        }
    }



}
