﻿using UnityEngine;
using System.Collections.Generic;

public class DeckManager : MonoBehaviour
{

    private List<CardData> currentDeck;
    private const int deckCount = 52;    

    public void NewDeck()
    {
        currentDeck = new List<CardData> ();
        int suit = 1;
        int rank = 1;
        for (int i = 0; i < deckCount; i++)
        {
            if(rank % 14 == 0)
            {
                suit++;
                rank = 1;
            }
            currentDeck.Add(new CardData(suit, rank, true));          
            rank++;
        }
    }

    public CardData GetRandomCard()
    {
        int randomNum = Random.Range(0 , currentDeck.Count);
        CardData cardGive = currentDeck[randomNum];
        currentDeck.RemoveAt(randomNum);
        return cardGive;
    }
    
}
