﻿using System;

[Serializable]
public struct CardData
{
    public eCardSuit suitCard;
    public int rankCard;
    public bool isClose;
    public CardData(int suit, int rank, bool close)
    {
        suitCard = (eCardSuit)suit;
        rankCard = rank;
        isClose = close;
    }
}

[Serializable]
public struct DeckInfo
{
    public CardData[] DeckData;
}

