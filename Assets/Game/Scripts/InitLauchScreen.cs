﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using UnityEngine.Advertisements;
using GooglePlayGames.BasicApi;
using GooglePlayGames;

public class InitLauchScreen : Photon.PunBehaviour
{
    public Button logIn;
    public Button logGuest;
    public GameObject panelLoad;
    public InputField InputName;
 //Apple App Store: 2590819
//Google Play Store: 2590818
//Mi GameCenter: 2590817
#if UNITY_IOS
     private string gameId = "2590819";
#elif UNITY_ANDROID
    private string gameId = "2590818";
#endif  

    // public this for initialization
    void Start()
    {
        panelLoad.SetActive(true);
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        PhotonNetwork.ConnectUsingSettings("v4.2");
        logIn.onClick.AddListener(OnLoginFacebook);
        logGuest.onClick.AddListener(OnLoginGuest);
        InputName.onEndEdit.AddListener(SaveNewName);
        Advertisement.Initialize(gameId);
        PlayerSave.singleton.ClearMoneyBeforeGame();
    }

    public void CallBackSocial(bool isConnected)
    {

    }    

    public override void OnConnectedToPhoton()
    {
        base.OnConnectedToPhoton();     
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            panelLoad.SetActive(false);
            FB.ActivateApp();
        }
    }

   

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            panelLoad.SetActive(false);
            FB.ActivateApp();
        }
        else
            Debug.Log("Failed to Initialize the Facebook SDK");
    }

    private void OnLoginGuest()
    {
        logGuest.interactable = false;
        FB.LogOut();
        PlayerSave.singleton.SaveFacebookId(null);
        PlayerSave.singleton.Avatar = null;
        InputName.gameObject.SetActive(true);
    }

    private void OnLoginFacebook()
    {
        logIn.interactable = false;
        logGuest.interactable = false;
        panelLoad.SetActive(true);
        if (!FB.IsLoggedIn)
        {
            //   var perms = new List<string>() { "public_profile", "email", "user_location"};
            var perms = new List<string>() { "public_profile", "email" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }
        else
        {
            FB.API("me?fields=first_name", HttpMethod.GET, NameCallBack);
            FB.API(PlayerSave.singleton.GetFacebookId() + "/picture?width=50&height=50", HttpMethod.GET, AvatarCallBack);
        }
    } 

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn && result.Error == null)
        {
            var aToken = AccessToken.CurrentAccessToken;           
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            PlayerSave.singleton.SaveFacebookId(aToken.UserId);
            FB.API("me?fields=first_name", HttpMethod.GET, NameCallBack);           
         //   FB.API("me?fields=user_location", HttpMethod.GET, LocationCallBack);
            FB.API(PlayerSave.singleton.GetFacebookId() + "/picture?width=50&height=50", HttpMethod.GET, AvatarCallBack);
        }
        else
        {
            Debug.Log(result.Error);
        }
    }

    private void NameCallBack(IGraphResult result)
    {
        IDictionary<string, object> profile = result.ResultDictionary;
        PlayerSave.singleton.SaveNewName(profile["first_name"].ToString());        
    }

    private void LocationCallBack(IGraphResult result)
    {
        IDictionary<string, object> profile = result.ResultDictionary;
        PlayerSave.singleton.Savelocation(profile["user_location"].ToString());
    }

    private void AvatarCallBack(IGraphResult result)
    {
        PlayerSave.singleton.Avatar = result.Texture;
        LoadSceneMainMenu();
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void SaveNewName(string newName)
    {
        if (!string.IsNullOrEmpty(newName))
        {
            InputName.gameObject.SetActive(false);
            PlayerSave.singleton.SaveNewName(newName);
            LoadSceneMainMenu();
        }
        else
        {
            InputName.gameObject.SetActive(true);
        }
    }

    private void LoadSceneMainMenu()
    {
        panelLoad.SetActive(true);
        PlayerSave.singleton.ClearMoneyBeforeGame();      
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
	

}
