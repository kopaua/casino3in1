﻿using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine;
public class CustomMigratationHost : NetworkMigrationManager
{ 

    protected override void OnClientDisconnectedFromHost(NetworkConnection conn, out SceneChangeOption sceneChange)
    {
        //NetworkManager.singleton.StopClient();
        base.OnClientDisconnectedFromHost(conn, out sceneChange);   
        bool _youAreNewHost = false;
        PeerInfoMessage NewHostInfo = new PeerInfoMessage();
        FindNewHost(out NewHostInfo, out _youAreNewHost);       
        if (_youAreNewHost == true)
        {
            if (BecomeNewHost(NetworkManager.singleton.networkPort))
            {
                 FindObjectOfType<TeenPattiManager>().ReconnectHost();
            }
        }
        else
        {
            newHostAddress = NewHostInfo.address;
            Reset(oldServerConnectionId);
            NetworkManager.singleton.networkAddress = newHostAddress;
           if( NetworkManager.singleton.client.ReconnectToNewHost(newHostAddress, NetworkManager.singleton.networkPort))
            {
                FindObjectOfType<TeenPattiManager>().ReconnectHost();
            }
            else
            {
                NetworkManager.singleton.StopClient();
            }              
        }        
    }
    protected override void OnServerHostShutdown()
    {
        base.OnServerHostShutdown();
    }


}
