﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;
public class CustomNetwork : MonoBehaviour
{
    [SerializeField]
    private Button buttonStartGame;
    [SerializeField]
    private GameObject panelLoad;
    private const int stepFindingMatch = 10, maxMatch =100;
    private int currentstepMatch = 0;

   
    private void Awake()
    {
        buttonStartGame.onClick.AddListener(OnStartGameButton);   
    }

    public void OnStartGameButton()
    {
        panelLoad.SetActive(true);
        buttonStartGame.enabled = false;
        Invoke("StartMatchMaking",3);
    }   

    private void StartMatchMaking()
    {
        NetworkManager.singleton.StartMatchMaker();       
        NetworkManager.singleton.matchMaker.ListMatches(currentstepMatch, currentstepMatch + stepFindingMatch, "", true, 0, 0, OnInternetMatchList);
    }

    //call this method to request a match to be created on the server
    private void CreateInternetMatch()
    {
        NetworkManager.singleton.matchMaker.CreateMatch(Application.productName, NetworkManager.singleton.matchSize, true, "", "", "", 0, 0, OnInternetMatchCreate);
    }

    //this method is called when your request for creating a match is returned
    private void OnInternetMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            MatchInfo hostInfo = matchInfo;
            NetworkServer.Listen(hostInfo, 9000);
            NetworkManager.singleton.StartHost(hostInfo);
          
        }
        else
        {
            panelLoad.SetActive(false);
            buttonStartGame.enabled = true;
            Debug.LogError("Create match failed");
        }
    }  

    //this method is called when a list of matches is returned
    private void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if (matches.Count == 0)
            {
                CreateInternetMatch();
                return;
            }
            else
            {
                for (int i = 0; i < matches.Count; i++)
                {
                    if (matches[i].currentSize <= NetworkManager.singleton.matchSize && matches[i].currentSize > 0)
                    {
                        NetworkManager.singleton.matchMaker.JoinMatch(matches[i].networkId, "", "", "", 0, 0, OnJoinInternetMatch);
                        return;
                    }
                }

                if (matches.Count < currentstepMatch + stepFindingMatch || currentstepMatch >= maxMatch)
                {
                    Debug.Log("create server  matches");
                    CreateInternetMatch();
                    return;
                }

                currentstepMatch += stepFindingMatch;
                NetworkManager.singleton.matchMaker.ListMatches(currentstepMatch, currentstepMatch + stepFindingMatch, "", true, 0, 0, OnInternetMatchList);

            }
        }
        else
        {
            panelLoad.SetActive(false);
            buttonStartGame.enabled = true;
            Debug.LogError("Couldn't connect to match maker");
        }
    }

    //this method is called when your request to join a match is returned
    private void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        Invoke("CreateInternetMatch",12);     
        if (success)
        {
            Debug.Log("success join");
            MatchInfo hostInfo = matchInfo;
            NetworkManager.singleton.StartClient(hostInfo);
        }
        else
        {
            panelLoad.SetActive(false);
            buttonStartGame.enabled = true;
            Debug.LogError("Join match failed");
        }
    }

    
}
