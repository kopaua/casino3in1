﻿using UnityEngine;
using UnityEngine.UI;

public class MatchMakingPhoton : Photon.PunBehaviour
{
    public GameObject panelLoad;
    public GameObject PanelAfterGame;
    public Button buttonStart;
    public Button buttonNoLimit;
    public Button buttonJoker;
    // Use this for initialization
    void Start ()
    {
        buttonStart.onClick.AddListener(OnCreateOrJoinGameStandart);
        buttonNoLimit.onClick.AddListener(OnCreateOrJoinGameNoLimit);
        buttonJoker.onClick.AddListener(OnCreateOrJoinGameJoker);

        if (!PhotonNetwork.connected)
        {
            panelLoad.SetActive(true);
            PhotonNetwork.ConnectUsingSettings("v4.2");
        }
        else
        {
            panelLoad.SetActive(false);
            if (PlayerSave.singleton.GetMoneyBeforeGame() != 0)
                PanelAfterGame.SetActive(true);
            else
                DailyReward();
        }      
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Home) || Input.GetKeyDown(KeyCode.Escape))
        {
            PhotonNetwork.Disconnect();
            Application.Quit();
        }
    }

    private void DailyReward()
    {
        int lastDayConnected = PlayerPrefs.GetInt("LastDay", 0);
        int nowDay = System.DateTime.Now.Day;
        if (lastDayConnected != nowDay)
        {
            GameObject.Find("PanelDailyReward").transform.GetChild(0).gameObject.SetActive(true);
            int daysConnected = PlayerPrefs.GetInt("DaysConnected", 0);
            int dailyReward = 300;
            if (lastDayConnected + 1 == nowDay)
            {
                daysConnected++;
                if (daysConnected == 1)
                    dailyReward = 500;
                else if (daysConnected == 2)
                    dailyReward = 900;
                else if (daysConnected == 3)
                    dailyReward = 1200;
                else if (daysConnected == 4)
                    dailyReward = 1500;
                else if (daysConnected > 4)
                    dailyReward = 2000;
            }
            else
            {
                daysConnected = 0;
            }
            GameObject.Find("PanelDailyReward/Panel/TextReward").GetComponent<Text>().text = dailyReward.ToString();
            PlayerSave.singleton.SaveNewMoney(PlayerSave.singleton.GetCurrentMoney() + dailyReward);
            PlayerPrefs.SetInt("DaysConnected", daysConnected);
            PlayerPrefs.SetInt("LastDay", nowDay);
        }
    }

    public override void OnConnectedToPhoton()
    {
        base.OnConnectedToPhoton();
        panelLoad.SetActive(false);
    }

    private void OnCreateOrJoinGameStandart()
    {
        CreateOrJoinGame(eTable.Standart);      
    }

    private void OnCreateOrJoinGameNoLimit()
    {
        CreateOrJoinGame(eTable.NoLimit);     
    }

    private void OnCreateOrJoinGameJoker()
    {
        CreateOrJoinGame(eTable.Joker);     
    }

    private void CreateOrJoinGame(eTable nameRoom)
    {
        Invoke("ForceQuit", 8);
        panelLoad.SetActive(true);
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        PlayerSave.singleton.currentTable = nameRoom;
        for (int i = 0; i < rooms.Length; i++)
        {
            if (rooms[i].Name.Contains(nameRoom.ToString()) && rooms[i].PlayerCount < rooms[i].MaxPlayers)
            {
                PhotonNetwork.JoinRoom(rooms[i].Name);
                return;
            }
        }
        RoomOptions roomOption = new RoomOptions() { IsVisible = true, MaxPlayers = 5 };
        PhotonNetwork.CreateRoom(nameRoom.ToString() + rooms.Length + 1, roomOption, TypedLobby.Default);
    }

    private void ForceQuit()
    {
        panelLoad.SetActive(false);
    }

    public override void OnDisconnectedFromPhoton()
    {
        base.OnDisconnectedFromPhoton();
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        base.OnPhotonRandomJoinFailed(codeAndMsg);
        RoomOptions roomOption = new RoomOptions() { IsVisible = true, MaxPlayers = 5 };
        PhotonNetwork.CreateRoom(null, roomOption, TypedLobby.Default);     
    }    

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        base.OnPhotonJoinRoomFailed(codeAndMsg);      
    }   

    public override void OnCreatedRoom()
    {
        CancelInvoke("ForceQuit");
        PlayerSave.singleton.SaveMoneyBeforeGame();
        base.OnCreatedRoom();
        PhotonNetwork.LoadLevel(2);
    }

    public override void OnJoinedRoom()
    {
        CancelInvoke("ForceQuit");
        PlayerSave.singleton.SaveMoneyBeforeGame();
        base.OnJoinedRoom();
        PhotonNetwork.LoadLevel(2);     
    }





}
